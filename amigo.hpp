#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"
#include <string>

class amigo:public Pessoa{
	private:
	string aniversario;
	string endereco;

	public:
	amigo();
	amigo(string nome, string idade, string telefone, string aniversario, string endereco);

	string getAniversario();
	void setAniversario(string aniversario);
	string getEndereco();
	void setEndereco(string endereco);
};

#endif
