#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa(){
	this->nome = "";
	this->idade = "";
	this->telefone = "";
}

Pessoa::Pessoa(string nome,string idade, string telefone) {
        setNome(nome);
        setIdade(idade);
        setTelefone(telefone);
}

string Pessoa::getNome(){
	return nome;
}

void Pessoa::setNome(string nome){
	this->nome = nome;
}

string  Pessoa::getIdade() {
	return idade;
}

void Pessoa::setIdade(string idade) {
	this->idade = idade;
}

string Pessoa::getTelefone(){
	return telefone;
}
void Pessoa::setTelefone(string telefone){
	this->telefone = telefone;
}

