#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"

class contato : public Pessoa{
	private:
	string email;

	public:
	contato();
	contato(string nome, string idade, string telefone,string email);

	string getEmail();
	void setEmail(string email);
};

#endif
